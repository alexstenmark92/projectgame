import random
import items
from collections import OrderedDict


class Enemy:
    def __init__(self):
        self.name = None
        self.hp = None
        raise NotImplementedError("Raw Enemies aren't allowed to be used. Please use an actual enemy.")
    
    def __str__(self):
        return self.name

    def is_alive(self):
        return self.hp > 0


class WeakEnemy(Enemy):
    def __init__(self):
        self.name = None
        self.hp = None
        self.item_claimed = False
        drops = OrderedDict()
        drops[0.05] = [items.WeakHealingPotion]
        drops[0.35] = [items.Dagger, items.LoafBread]
        drops[0.90] = [items.Stick]

        for chance, itemlist in drops.items():
            if chance >= random.random():
                self.item = random.choice(itemlist)()
                break
            else:
                self.item = items.LoafBread()


class MediumEnemy(Enemy):
    def __init__(self):
        self.name = None
        self.hp = None
        self.item_claimed = False
        drops = OrderedDict()
        drops[0.10] = [items.HealingPotion]
        drops[0.35] = [items.Dagger]
        drops[0.90] = [items.LoafBread]

        for chance, itemlist in drops.items():
            if chance >= random.random():
                self.item = random.choice(itemlist)()
                break
            else:
                self.item = items.LoafBread()


class StrongEnemy(Enemy):
    def __init__(self):
        self.name = None
        self.hp = None
        self.item_claimed = False
        drops = OrderedDict()
        drops[0.05] = [items.WeakHealingPotion]
        drops[0.35] = [items.Dagger, items.LoafBread]
        drops[0.90] = [items.Stick]

        for chance, itemlist in drops.items():
            if chance >= random.random():
                self.item = random.choice(itemlist)()
                break
            else:
                self.item = items.LoafBread()


#Tier1 enemies
class Goblin(WeakEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Goblin'
        self.hp = 15
        self.damage = 5


class Fox(WeakEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Fox'
        self.hp = 15
        self.damage = 3


class GiantRat(WeakEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Giant Rat'
        self.hp = 13
        self.damage = 4


class YoungWolf(WeakEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Young Wolf'
        self.hp = 20
        self.damage = 6


#Tier1 Boss(MiniBoss)
class WhiteWolf(Enemy):
    def __init__(self):
        self.name = 'White Wolf'
        self.hp = 50
        self.damage = 12
        self.item = items.ShortSword()
        self.item_claimed = False


#Tier2 Enemies
class GoblinWarrior(MediumEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Goblin Warrior'
        self.hp = 25
        self.damage = 6

class Orc(MediumEnemy):
    def __init__(self):
        super().__init__()
        self.name = 'Orc'
        self.hp = 35
        self.damage = 8


#Tier3 enemies
class OrcWarrior(Enemy):
    def __init__(self):
        self.name = 'Orc Warrior'
        self.hp = 30
        self.damage = 10


class StoneGolem(Enemy):
    def __init__(self):
        self.name = 'Stone Golem'
        self.hp = 80
        self.damage = 15


#Boss Tier Enemies
class Giant(Enemy):
    def __init__(self):
        self.name = 'Giant'
        self.hp = 200
        self.damage = 30
