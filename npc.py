import items

class NonPlayableCharacter():
    def __init__(self):
        self.name = None
        raise NotImplementedError('Do not create raw NPC objects.')

    def __str__(self):
        return self.name


class Merchant(NonPlayableCharacter):
    def __init__(self):
        self.name = 'Merchant'
        self.gold = 100
        self.inventory = [items.LoafBread(),
                          items.LoafBread(),
                          items.LoafBread(),
                          items.HealingPotion(),
                          items.HealingPotion()]


class Blacksmith(NonPlayableCharacter):
    def __init__(self):
        self.name = 'Blacksmith'
        self.gold = 100
        self.inventory = [items.LeatherArmour(),
                          items.ScaleArmour(),
                          items.ShortSword()]

##########################################################################################

class Innkeeper(NonPlayableCharacter):
    def __init__(self):
        self.name = 'Innkeeper'
        self.gold = 100
        self.inventory = None
        raise NotImplementedError('not in current version')


class Elder(NonPlayableCharacter):
    def __init__(self):
        self.name = 'Elder'
        self.gold = 100
        self.inventory = None
        raise NotImplementedError('Not in current version')
