from player import Player
import world
from importlib import reload

def play():
    print('''
                    The Dark Hedge
----------------------------------------------------------
Can you find your way through this dark mysterious forest.
Or will you forever haunt a dark corner of the forest...
----------------------------------------------------------
''')
    world.parse_world_dsl()
    player = Player()
    while player.is_alive():
        room = world.tile_at(player.x, player.y)
        print(room.intro_text())
        room.modify_win(player)
        if player.victory:
            print('You have won!')
            break
        print('Choose an action:')
        room.choose_action(player)
        room.modify_player(player)

    if not player.is_alive():
            print('You have died.')
    if input('Play again?(Y/N)').lower().startswith('y'):
        #reload(player)
        reload(world)
        play()
    
if __name__ == '__main__':
    play()