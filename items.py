class Item:
    def __init__(self):
        self.attributes ={'name' : None, 'description' : None, 'damage' : None, 'armour': None, 'healing_value': None}
        #raise NotImplementedError("Don't create raw a Weapon object.")

    def __str__(self):
        if self.attributes.get('healing_value', 0):
            return f'{self.attributes.get("name", 0)} (+ {self.attributes.get("healing_value", 0)})'       
        else:
            return f'{self.attributes.get("name", 0)}'


class Stick(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Stick'
        self.attributes['description'] = 'A simple but quite effective gnarly old branch, well suited for bludgeoning.'
        self.attributes['damage'] = 5
        self.attributes['value'] = 1


class Dagger(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Dagger'
        self.attributes['description'] = 'A worn, but still sharp dagger, you can just see how well it will stab your foes.'
        self.attributes['damage'] = 10
        self.attributes['value'] = 20


class ShortSword(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Short Sword'
        self.attributes['description'] = 'A worn, but still sharp dagger, you can just see how well it will stab your foes.'
        self.attributes['damage'] = 15
        self.attributes['value'] = 100


class Falchion(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Falchion'
        self.attributes['description'] = 'A sharpened blade and powerful blade, it is rare to see a more powerful weapon than this. '
        self.attributes['damage'] = 20
        self.attributes['value'] = 100


class LongSword(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Long Sword'
        self.attributes['description'] = '''
An ornate long sword, that you could wield with one or two hands, 
it's potential limitless in a skilled man's hands.'''
        self.attributes['damage'] = 30
        self.attributes['value'] = 150


class Gambeson(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Gambeson'
        self.attributes['description'] = "A cheap yet efficient armour made out of padded cloth, often worn beneath plate and mail armour."
        self.attributes['armour'] = 1
        self.attributes['value'] = 10


class LeatherArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Leather Armour'
        self.attributes['description'] = 'Another cheap and reasonably effective armour, made out of boiled leather and it tends to be a little awkward to be worn.'
        self.attributes['armour'] = 2
        self.attributes['value'] = 15


class ScaleArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Scale Armour'
        self.attributes['description'] = """An uncommon defensive layer made out of small scales,
                              orchestrated in a arrangement like a snakes skin. 
                              It effectively out performs it's forerunners."""
        self.attributes['armour'] = 4
        self.attributes['value'] = 40


class LamellarArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Lamellar Armour'
        self.attributes['description'] = "An uncommon armour made out of little scales, arranged in a formation like a snakes skin, though it handidly out performs it"
        self.attributes['armour'] = 5
        self.attributes['value'] = 40


class MailArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Mail armour'
        self.attributes['description'] = "An uncommon armour made out of little scales, arranged in a formation like a snakes skin, though it handidly out performs it"
        self.attributes['armour'] = 7
        self.attributes['value'] = 40


class BrigandineArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Brigandine Armour'
        self.attributes['description'] = "An uncommon armour made out of little scales, arranged in a formation like a snakes skin, though it handidly out performs it"
        self.attributes['armour'] = 9
        self.attributes['value'] = 40


class FullPlateArmour(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Full Plate Armour'
        self.attributes['description'] = "The heaviest of all armours, conjointly the rarest. It's value is difficult to measure and it'll unquestionably protect you from all but the heaviest blows."
        self.attributes['armour'] = 15
        self.attributes['value '] = 1200


class LoafBread(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Loaf of Bread'
        self.attributes['healing_value'] = 10
        self.attributes['value'] = 12


class WeakHealingPotion(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Weak Healing Potion'
        self.attributes['healing_value'] = 25
        self.attributes['value'] = 40


class HealingPotion(Item):
    def __init__(self):
        super().__init__()
        self.attributes['name'] = 'Healing Potion'
        self.attributes['healing_value'] = 50
        self.attributes['value'] = 75
