import items
import world
import random
from collections import OrderedDict


class Player:
    def __init__(self):
        self.inventory = [items.Stick(),items.Gambeson(), items.LoafBread()]
        self.x = world.start_tile_location[0]
        self.y = world.start_tile_location[1]
        self.hp = 100
        self.maxhp = 100
        self.gold = 5
        self.victory = False

    def is_alive(self):
        return self.hp > 0
    
    def move(self, direction):
        if direction == 'north':
            self.y -=1
        if direction == 'south':
            self.y +=1
        if direction == 'east':
            self.x +=1
        if direction == 'west':
            self.x -=1

    def print_inventory(self):
        print(f'Player HP: {self.hp}\n')
        print('Inventory:')
        for item in self.inventory:
            print(f'\n* {str(item)}')
            if item.attributes.get('description', 0):
                print(f'----------------\n{item.attributes.get("description", 0)}')
        print(f'\nGold: {self.gold}')

    def most_powerful_weapon(self):
        max_damage = 0
        best_weapon = None
        for item in self.inventory:
            w = item.attributes.get('damage', 0)
            if w and w > max_damage:
                best_weapon = item
                max_damage = item.attributes.get('damage', 0)
        return best_weapon

    def best_armour(self):
        max_armour = 0
        best_armour = None
        for item in self.inventory:
            a = item.attributes.get('armour', 0)
            if a and a > max_armour:
                best_armour = item
                max_armour = a
        return best_armour

    def attack(self):
        best_weapon = self.most_powerful_weapon()
        room = world.tile_at(self.x, self.y)
        enemy = room.enemy
        damage = best_weapon.attributes.get('damage', 0)
        print(f'You use {best_weapon} against {enemy.name}.')
        damagerange = OrderedDict()
        damagerange[0.05] = [round(damage * 1.5)] #Rounding the damage so we get integer values
        damagerange[0.75] = [damage]
        damagerange[0.95] = [round(damage * 0.75)] 

        for chance, damagelist in damagerange.items():
            if chance >= random.random():
                damage = random.choice(damagelist)
                break
        else:
            damage = 0
        if damage > 0:
            print(f'You deal {damage}')
            enemy.hp -= damage
            if not enemy.is_alive():
                print(f'You killed {enemy.name}')
            else:
                print(f'{enemy.name} HP is {enemy.hp}.')
        else:
            print(f'You swing and you miss {enemy.name}')

    def loot(self):
        room = world.tile_at(self.x, self.y)
        item = room.enemy.item
        self.inventory.append(item)
        print(f'You picked up {item.attributes.get("name", 0)}')
        room.enemy.item_claimed = True
    
    def open_chest(self):
        room = world.tile_at(self.x, self.y)
        room.check_chest(self)


    def flee(self):
        room = world.tile_at(self.x, self.y)
        notallowed = ['win', 'mini']
        if random.random() <= 0.75:
            tile_n = world.tile_at(room.x, room.y - 1)
            tile_s = world.tile_at(room.x, room.y + 1)
            tile_e = world.tile_at(room.x + 1, room.y)
            tile_w = world.tile_at(room.x - 1, room.y)

            choices = [(tile_n, 'north'), (tile_s, 'south'), (tile_e, 'east'), (tile_w, 'west')]
            choices = [y for x, y in choices if x is not None and x.tile_type not in notallowed]
            a = random.choice(choices)
            print(f'You fled {a}')
            self.move(a)
            return

        else:
            print(f'You failed to flee from {room.enemy.name}')
            return
        
    def heal(self):
        consumables = []
        valid = False

        for item in self.inventory: #removes items without healing value
            if item.attributes.get('healing_value', 0):
                consumables.append(item)
            else:
                continue

        if not consumables:             #If theres no items in your inventory with healing value it tells you have none.
            print('You are out of healing items.')
            return
        print('Choose an item to heal: ')
        for i, item in enumerate(consumables, 1): #Lists out items with values and starting on 1, rather than 0
            print(f'{i} {item}')
            
        while not valid:
            choice = input('Choose a number or (q): ')
            if choice.lower() == 'q':
                print('Exiting heal menu')
                return
            try:   
                to_eat = consumables[int(choice) -1]
                self.hp = min(self.maxhp, self.hp + to_eat.attributes.get('healing_value', 0))
                self.inventory.remove(to_eat)
                print(f'You heal for {to_eat.attributes.get("healing_value", 0)} and now have {self.hp}HP')
                valid = True
            except(ValueError, IndexError):
                print('Not a valid choice, try again.')
    
    def trade(self):
        room = world.tile_at(self.x, self.y)
        room.check_if_trade(self)

    def quit(self):
        exit('Exiting the game.')
