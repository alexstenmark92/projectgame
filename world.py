from collections import OrderedDict
import random
import enemies
import npc
import player
import items


class MapTile:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def intro_text(self):
        raise NotImplementedError ('Raw object map.')

    def modify_win(self, player):
        pass

    def modify_player(self, player):
        pass
    
    def tile_actions(self, player):
        actions = OrderedDict()
        if player.print_inventory:
            self.add_action(actions, 'i', (player.print_inventory,), 'Print inventory')
        if player.hp < player.maxhp:
            self.add_action(actions, 'h', (player.heal,), 'Heal')
        if tile_at(self.x, self.y -1):
            self.add_action(actions, 'n', (player.move, 'north'), 'Go North')
        if tile_at(self.x, self.y + 1):
            self.add_action(actions, 's', (player.move, 'south'), 'Go South')
        if tile_at(self.x +1, self.y):
            self.add_action(actions, 'e', (player.move, 'east'), 'Go East')
        if tile_at(self.x -1, self.y):
            self.add_action(actions, 'w', (player.move, 'west'), 'Go West')
        if player.quit:
            self.add_action(actions, 'q', (player.quit,), 'Quit the game') 
        return actions

    def add_action(self, action_dict, hotkey, action, name):
        action_dict[hotkey.lower()] = action
        print(f'{hotkey}: {name}')

    def choose_action(self, player):
        action = None
        while not action:
            available_actions = self.tile_actions(player)
            action_input = input('Action: ').lower()
            action = available_actions.get(action_input)
            if action:
                try:
                    action[0](action[1])
                except IndexError:
                    action[0]()
            else:
                print('Not a valid action.')


class StartTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'start'

    def intro_text(self):
        return '''
        You find yourself, surrounded on all sides by 
        the Dark Hedges, filled with dangers...

        It's like it came from a fairy tale to 
        frighten little children to bed at night.
        '''


class VictoryTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'win'
    def modify_win(self, player):
        player.victory = True

    def intro_text(self):
        return '''
You have reached the end of the forest but have you won?.
        '''


class EnemyTile(MapTile):
    def __init__(self, x, y):
        self.enemy = None
        super().__init__(x, y)

    def tile_actions(self, player):
        actions = OrderedDict()
        if self.enemy.is_alive():
            self.add_action(actions, 'a', (player.attack,), 'Attack')
            self.add_action(actions, 'f', (player.flee,), 'Flee')
            if player.hp < player.maxhp:
                self.add_action(actions, 'h', (player.heal,), 'Heal')
            if player.quit:
                self.add_action(actions, 'q', (player.quit,), 'Quit the game')
        else:
            if not self.enemy.item_claimed and not self.enemy.is_alive():
                self.add_action(actions, 'l',(player.loot,), 'Loot')
            actions.update(MapTile.tile_actions(self, player).items())
        return actions

    def intro_text(self):
        if self.enemy.is_alive():
            return f'You see {self.enemy.name} and prepare yourself.'
        
        elif not self.enemy.item_claimed and not self.enemy.is_alive():
            return f'You see a {self.enemy.item.attributes.get("name", 0)} on the ground.'
        else:
            return f'You see the corpse of a {self.enemy.name}'

    def modify_player(self, player):
        if self.enemy.is_alive():
            best_armour = player.best_armour()
            damage = self.enemy.damage - best_armour.attributes.get('armour', 0)
            if damage > 0:
                player.hp = player.hp - damage
                print(f'enemy does {damage}, You now have {player.hp} HP left.')
            else:
                print(f'{self.enemy.name} failed to damage you through your armor.')


class WeakEnemyTile(EnemyTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'weak'
        monster = OrderedDict()
        monster[0.25] = [enemies.Fox]
        monster[0.50] = [enemies.GiantRat]
        monster[0.70] = [enemies.YoungWolf]

        for chance, enemylist in monster.items():
            if chance >= random.random():
                self.enemy = random.choice(enemylist)()
                break
        else:
            self.enemy = enemies.Goblin()


class MediumEnemyTile(EnemyTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'medium'
        drops = OrderedDict()
        drops[0.25] = [enemies.GoblinWarrior]
        drops[0.50] = [enemies.Orc]
        drops[0.75] = [enemies.GoblinWarrior]

        for chance, enemylist in drops.items():
            if chance >= random.random():
                self.enemy = random.choice(enemylist)()
                break
        else:
            self.enemy = enemies.Orc()


class StrongEnemyTile(EnemyTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'strong'
        drops = OrderedDict()
        drops[0.25] = [enemies.Fox]
        drops[0.50] = [enemies.GiantRat]
        drops[0.75] = [enemies.YoungWolf]

        for chance, enemylist in drops.items():
            if chance >= random.random():
                self.enemy = random.choice(enemylist)()
                break
        else:
            self.enemy = enemies.Goblin()


class MiniBossTile(EnemyTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'mini'
        self.enemy = enemies.WhiteWolf()


class BossEnemyTile(MapTile):
    def __init__(self, x, y):
        self.loot_claimed = False
        super().__init__(x, y)
        self.enemy = enemies.Giant()
        self.alive_text = 'Giant'
        self.dead_text = 'Dead Giant?'


class VillageTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.inkeeper = npc.Innkeeper()
        self.elder = npc.Elder()

    def intro_text(self):
        raise NotImplementedError('Dont use village tile yet')
        # return '''
        # A quiet little village on the edge of the river bank.
        # You can see a little inn, maybe you should take a look?
        # You see a dark plume of smoke rising from another nearby building maybe they have a blacksmith?
        # You also see a gathering of a few people and hearing a voice, Should you check it out?
        # '''

    def explore_village(self):
        raise NotImplementedError('Maybe Inkeeper and Elder here?')
        
#        def modify_player(self, player):
            #rest a day at the in, maybe could respawn enemies potentially, or maybe not with this? hmmmm...


class GenericTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)

    def intro_text(self):
        return 'This is a generic tile and you prepare to move on.'


class TraderTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.tile_type = 'trade'
        self.r = random.random()
        if 0.5 >= self.r:
            self.trader = npc.Merchant()
        else:
            self.trader = npc.Blacksmith()
    
    def tile_actions(self, player):
        actions = OrderedDict()
        self.add_action(actions, 't', (player.trade,), 'Trade')
        actions.update(MapTile.tile_actions(self, player).items()) 
        return actions        

    def trade(self, buyer, seller):

        while True:
            for i, item in enumerate(seller.inventory, 1):
                if seller == self.trader:
                    print(f'{i}. {item.attributes.get("name", 0)} - {round(item.attributes.get("value", 0) * 1.1)} Gold')
                else:
                    print(f'{i}. {item.attributes.get("name", 0)} - {round(item.attributes.get("value", 0) * 0.9)} Gold')

            user_input = input('Choose an item or press Q to exit: ').lower()
            if user_input == 'q':
                return
            else:
                try: 
                    choice = int(user_input)
                    to_swap = seller.inventory[choice - 1]
                    self.swap(seller, buyer, to_swap)
                #    return
                except (ValueError, IndexError):
                    print('Not a valid choice.')


    def check_if_trade(self, player):
        while True:
            print('Would you like to (B)uy, (S)ell or (Q)uit?')
            user_input = input().lower()
            if user_input == 'q':
                return
            elif user_input == 'b':
                print("Here's whats available to buy: ")
                self.trade(buyer=player, seller=self.trader)
            elif user_input == 's':
                print("Here's whatss available to sell: ")
                self.trade(buyer=self.trader, seller=player)
            else:
                print('Not a valid choice.')

    def intro_text(self):
        if 0.5 >= self.r:
            text = 'merchant'
        else:
            text = 'blacksmith'
        return f'{text}'
    
    def swap(self, seller, buyer, item):
        if item.attributes.get('value', 0) > buyer.gold:
            print("That's too expensive")
            return
        seller.inventory.remove(item)
        buyer.inventory.append(item)
        if seller == self.trader:
            seller.gold = seller.gold + round(item.attributes.get('value', 0) * 1.1)
            buyer.gold = buyer.gold - round(item.attributes.get('value', 0) * 1.1)
        else:
            seller.gold = seller.gold + round(item.attributes.get('value', 0) * 0.9)
            buyer.gold = buyer.gold - round(item.attributes.get('value', 0) * 0.9)
        print('Trade Complete.')


class BoatTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        raise NotImplementedError('not done yet')


class TreasureRoomTile(MapTile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.treasure_claimed = False
        drops = OrderedDict()
        drops[0.05] = [items.WeakHealingPotion]
        drops[0.35] = [items.Dagger, items.LoafBread]
        drops[0.50] = [items.Stick]

        for chance, itemlist in drops.items():
            if chance >= random.random():
                self.item = random.choice(itemlist)()
                break
        else:
            self.item = None

    def intro_text(self):
        return 'You see an old and weathered treasure chest...'
        # if not self.treasure_claimed:
        #     return '''You see an empty treasure chest'''
        # else:
        #     return '''silly face'''


    def tile_actions(self, player):
        actions = OrderedDict()
        if not self.treasure_claimed:
            self.add_action(actions, 'o', (player.open_chest,), 'Open the chest!')
            actions.update(MapTile.tile_actions(self, player).items())
        else:
            actions.update(MapTile.tile_actions(self, player).items())
        return actions

    def check_chest(self, player):
        input()
        print('You walk up towards the chest... and take a long look at it, to see if you can see what would be in it...')


    #         # self.item_claimed = True
            
    #         # print(f'You picked up {self.item}.')

    #         # player.inventory.append(self.item)

    # #     self.gold = random.randint(35, 100)
    # #     self.gold_claimed = False
    # #     super().__init__(x, y)
    
    # # def modify_player(self, player):
    # #     if not self.gold_claimed:
    # #         self.gold_claimed = True
    # #         player.gold = player.gold + self.gold
    # #         print(f'+ {self.gold} gold added.')
    
    # def intro_text(self):
    #     if self.item_claimed:
    #         return '''
    #         There is no gold left. Placeholder text.
    #         '''
    #     else:
    #         return ''' 
    #         You see a little unremarkable chest,
    #         what could it possibly contain?
    #         '''

world_map = []

world_dsl = '''
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |TT|ME|  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |TR|ST|WE|  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |MB|VT|  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |VT|WE|  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |VT|  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |  |  |  |MB|VT|  |  |  |  |  |  |  |  |  |  |  |  |  |
|WE|WE|  |WE|WE|ME|  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |WE|  |WE|TT|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|WE|WE|WE|WE|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |  |WE|  |WE|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |WE|WE|WE|WE|WE|WE|  |  |  |  |  |  |  |  |  |  |  |  |  |
|  |WE|  |  |  |  |WE|  |  |  |  |  |  |  |  |BE|VT|  |  |  |
|  |WE|  |TT|  |  |WE|  |  |  |  |  |  |  |  |  |  |  |  |  |
|WE|WE|WE|WE|  |  |WE|  |  |  |  |  |  |  |  |  |  |  |  |  |
'''

tile_type_dict = {'VT': VictoryTile,
                  'WE': WeakEnemyTile,
                  'ME': MediumEnemyTile,
                  'SE': StrongEnemyTile,
                  'BE': BossEnemyTile,
                  'ST': StartTile,
                  'TT': TraderTile,
                  'MB': MiniBossTile,
                  'VI': VillageTile,
                  'TR': TreasureRoomTile,
                  'GT': GenericTile,
                  '  ': None}

def is_dsl_valid(dsl):
    if dsl.count('|ST|') != 1:
        return False
    if dsl.count('|VT|') == 0:
        return False
    if dsl.count('|BE|') == 0:
        return False
    lines = dsl.splitlines()
    lines = [l for l in lines if l]
    pipe_counts = [line.count('|') for line in lines]
    for count in pipe_counts:
        if count != pipe_counts[0]:
            return False
    return True

def parse_world_dsl():
    if not is_dsl_valid(world_dsl):
        raise SyntaxError('DSL is invalid.')
    dsl_lines = world_dsl.splitlines()
    dsl_lines = [x for x in dsl_lines if x]
    for y, dsl_row in enumerate(dsl_lines):
        row = []
        dsl_cells = dsl_row.split('|')
        dsl_cells = [c for c in dsl_cells if c]
        for x, dsl_cell in enumerate(dsl_cells):
            tile_type = tile_type_dict[dsl_cell]
            if tile_type == StartTile:
                global start_tile_location
                start_tile_location = x, y
            row.append(tile_type(x, y) if tile_type else None)
        world_map.append(row)

def tile_at(x, y):
    if x < 0 or y < 0:
        return None
    try:
        return world_map[y][x]
    except IndexError:
        return None
